###Mail Sender
   
**Functionality:**
   
   + receive JMS-message from activeMQ server
   + send messages to e-mail
   + add documents to mongodb  
   
   **Tools:**  
   JDK 7,8, Spring-boot, jms/activemq, spring data/mongodb, JavaMailSender, Maven, Git/Bitbucket, IntelliJIDEA.  
   
   --  
**Makhova Ekaterina**  
Training getJavaJob  
http://www.getjavajob.com