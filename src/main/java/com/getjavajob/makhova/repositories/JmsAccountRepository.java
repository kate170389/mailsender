package com.getjavajob.makhova.repositories;

import com.getjavajob.makhova.common.JMSFriendRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface JmsAccountRepository extends MongoRepository<JMSFriendRequest, String> {
}