package com.getjavajob.makhova;

import com.getjavajob.makhova.common.JMSFriendRequest;
import com.getjavajob.makhova.repositories.JmsAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class JmsAccountReceiver {
    private final JmsAccountRepository repository;
    private final EmailService emailService;

    @Autowired
    public JmsAccountReceiver(JmsAccountRepository repository, EmailService emailService) {
        this.repository = repository;
        this.emailService = emailService;
    }

    @JmsListener(destination = "test")
    public void receiveMessage(JMSFriendRequest account) {
        System.out.println("Received <" + account + ">");
        JMSFriendRequest savedAccount = repository.save(account);
        String textMessage="You have new request from acoount with e-mail:"+savedAccount.getEmailFrom();
        emailService.prepareAndSend(savedAccount.getEmailTo(),textMessage);
    }
}