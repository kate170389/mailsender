package com.getjavajob.makhova.common;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = JMSFriendRequest.COLLECTION_NAME)
public class JMSFriendRequest implements Serializable {
    static final String COLLECTION_NAME = "accounts";

    @Id
    private String id;
    private String emailFrom;
    private String emailTo;
    private String date;

    public JMSFriendRequest() {
    }

    public JMSFriendRequest(String emailFrom, String emailTo, String date) {
        this.emailFrom = emailFrom;
        this.emailTo = emailTo;
        this.date = date;
    }

    public JMSFriendRequest(String emailFrom, String date) {
        this.emailFrom = emailFrom;
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }

    public String getEmailTo() {
        return emailTo;
    }

    public void setEmailTo(String emailTo) {
        this.emailTo = emailTo;
    }

    @Override
    public String toString() {
        return "JMSFriendRequest{" +
                "id='" + id + '\'' +
                ", emailFrom='" + emailFrom + '\'' +
                ", emailTo='" + emailTo + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}